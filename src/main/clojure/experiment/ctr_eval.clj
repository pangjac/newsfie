(ns experiment.ctr-eval
  (:require [clojure.core.matrix :as m]
            [lifestreams-db.ctr :as ctr]
            [lifestreams-db.crawler :as crawler]
            [clojure.java.jdbc :as sql]
            [lifestreams-db.postgres :refer [pooled-db]]
            [incanter.stats :as stats]
            [incanter.io :as io]
            [lifestreams-db.data :as data]
            [lifestreams-db.medium :as medium]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.core.matrix.linear :as ml]
            [cheshire.core :as json]
            [clojure.core.reducers :as r])
  (:import (org.joda.time DateTime))
  (:use [incanter.core]
        [incanter.charts]
        [experiment.analysis]))
(m/set-current-implementation :vectorz)

(defn rank-stories
  "Return story->rank map where lower the rank value the higher the rank"
  [story-prefs stories]
  (let [ranked-stories (->> story-prefs
                            (map vector stories)
                            (sort-by second)
                            (reverse)
                            (map first)
                            )
        ]
    (into {} (map vector ranked-stories (range))))
  )

(defn one-run []
  (let [users   (->> (sql/query @pooled-db [
                                            "SELECT id, twitter_screen_name as tw, medium_user_name as u
                                            FROM person WHERE twitter_screen_name IS NOT NULL"])
                     (filter #(crawler/twitter-name->twitter-profile (:tw %)))
                     )
        user->training (let [uid->stories (->> (sql/query @pooled-db
                                           ["SELECT medium_story.id AS id, person_id AS uid
                                            FROM medium_story
                                            INNER JOIN medium_like ON story_id = id
                                            WHERE person_id=ANY(?)
                                                  AND published_at < ?
                                                  AND text IS NOT NULL
                                                  AND upvote_count >= 1"
                                                           (map :id users) (DateTime. 2015 1 1 0 0)])
                                               (map (fn [{:keys [uid id]}] {uid [id]}))
                                               (apply merge-with concat)
                                               )]
                         (fn [{:keys [id]}]
                           (uid->stories id)
                           )
                         )
        user->testing (let [uid->stories (->> (sql/query @pooled-db
                                                         [
                                                          "SELECT medium_story.id AS id, person_id AS uid
                                                           FROM medium_story
                                                           INNER JOIN medium_like ON story_id = id
                                                           WHERE person_id=ANY(?)
                                                                 AND published_at >= ?
                                                                 AND text IS NOT NULL
                                                                 AND upvote_count >= 1"
                                                          (map :id users) (DateTime. 2015 1 1 0 0)])
                                              (map (fn [{:keys [uid id]}] {uid [id]}))
                                              (apply merge-with concat)
                                              )]
                        (fn [{:keys [id]}]
                          (uid->stories id)
                          )
                        )
        user->stories #(distinct (concat (user->training %) (user->testing %)))
        training-docs
        (->> (mapcat
               (fn [u] (user->training u))
               users)

             (distinct))
        testing-docs
        (->> (mapcat
               (fn [u] (user->testing u))
               users)
             (distinct))

        docs
        (concat training-docs testing-docs)
        testing-users (->> users
                           (filter (fn [u]
                                     (and (>= (count (user->training u)) 20) (>= (count (user->testing u)) 5))
                                     ))
                           (shuffle)
                           (take 5000)

                           )
        training-users (let [test-us (into #{} testing-users)]
                         (->> users
                              (filter #(not (contains? test-us %)))
                              )

                         )


        Theta (into {} (pmap #(vector % (crawler/story->topic %)) docs))
        Gamma (into {} (pmap #(vector % ((comp crawler/twitter-name->twitter-profile :tw) %)) training-users))
        user-doc (apply concat
                   (for [u training-users]
                     (for [s (user->stories u)]
                       [u s]
                       )

                     ))

        pmf
        (ctr/estimate
          (into {} (pmap #(vector % (m/zero-vector 200)) docs))
          (into {} (pmap #(vector % (m/zero-vector 200)) training-users))
          user-doc
          1
          0.01
          0.01
          0.01
          5
          )
        ours
        (ctr/estimate Theta
                      Gamma
                      user-doc
                      1
                      0.01
                      10
                      10
                      10
                      )


        ctr
        (ctr/estimate Theta
                      (into {} (pmap #(vector % (m/zero-vector 200)) training-users))
                      user-doc
                      1
                      0.01
                      10
                      0.01
                      5
                      )

        ]
    {:ours ours
     :ctr ctr
     :pmf pmf
     :training-docs        training-docs
     :testing-docs         testing-docs
     :testing-users        testing-users
     :training-users       training-users
     :theta Theta
     :gamma Gamma
     :user->training user->training
     :user->testing user->testing
     }

    )
  )


(defn fold-in [profile V XX likes lambda_u]
  (let [a 1 b 0.01
        gamma-u profile
        num-factors (m/column-count V)
        a-minus-b (- a b)
        A (reduce (fn [A doc-id]
                    (let [v (m/row-matrix (m/get-row V doc-id))]
                      (m/add A (m/mul (m/mmul (m/transpose v) v) a-minus-b))
                      )
                    ) XX likes)
        x (reduce (fn [x doc-id]
                    (let [v (m/get-row V doc-id)]
                      (m/add x (m/mul v a))
                      )
                    ) (m/zero-vector num-factors) likes)
        x (m/add x (m/mul gamma-u lambda_u))
        A (m/add A (m/mul (m/identity-matrix num-factors) lambda_u))
        ]
    (m/to-vector (ml/solve (m/mul A 1e6) (m/mul x 1e6)))
    )
  )
(defn analysis [V ctr-V pmf-V trainning-docs tranning-users testing-docs testing-users]
  (let [b 0.01
        XX (->>
                (map second V)
                (matrix)
                (pmap #(m/outer-product % %) )
                (apply m/add)
                (m/mul b))
        ctr-XX (->>
                 (map second ctr-V)
                 (matrix)
                 (pmap #(m/outer-product % %) )
                (apply m/add)
                (m/mul b))
        pmf-XX (->>
                 (map second pmf-V)
                 (matrix)
                 (pmap #(m/outer-product % %))
                 (apply m/add)
                 (m/mul b))
        user-id->testing-stories (->> (sql/query @pooled-db
                                      [
                                       "SELECT medium_story.id AS id, person_id AS uid
                                        FROM medium_story
                                        INNER JOIN medium_like ON story_id = id
                                        WHERE person_id=ANY(?)
                                              AND published_at >= ?
                                              AND published_at < ?
                                              AND text IS NOT NULL
                                              AND upvote_count >= 10"
                                       (map :id testing-users) (DateTime. 2015 1 1 0 0)])
                           (map (fn [{:keys [uid id]}] {uid [id]}))
                           (apply merge-with concat)
                           )
        user-id->training-stories (->> (sql/query @pooled-db
                                                 [
                                                  "SELECT medium_story.id AS id, person_id AS uid
                                                    FROM medium_story
                                                    INNER JOIN medium_like ON story_id = id
                                                    WHERE person_id=ANY(?)
                                                          AND published_at < ?
                                                          AND text IS NOT NULL
                                                          AND upvote_count >= 10 ORDER BY person_id, published_at"
                                                  (map :id testing-users) (DateTime. 2015 1 1 0 0)])
                                      (map (fn [{:keys [uid id]}] {uid [id]}))
                                      (apply merge-with concat)
                                      )
        testing-docs-set (into #{} testing-docs)
        training-docs-set (into #{} trainning-docs)
        testing-doc->index (into {} (map #(vector %1 %2) testing-docs (range)))
        training-doc->index (into {} (map #(vector %1 %2) trainning-docs (range)))


        testing-V (m/matrix (map #(or (V %) (crawler/story->topic %)) testing-docs))
        training-V (m/matrix (map #(or (V %) (crawler/story->topic %)) trainning-docs))

        ctr-testing-V (m/matrix (map #(or (ctr-V %) (crawler/story->topic %)) testing-docs))
        ctr-training-V (m/matrix (map #(or (ctr-V %) (crawler/story->topic %)) trainning-docs))


        pmf-testing-V (m/matrix (map #(or (pmf-V %) (crawler/story->topic %)) testing-docs))
        pmf-training-V (m/matrix (map #(or (pmf-V %) (m/zero-vector 200)) trainning-docs))

        story->frequencies (->> (sql/query @pooled-db
                                           ["SELECT medium_story.id AS id, person_id AS uid
                                                   FROM medium_story
                                                   INNER JOIN medium_like ON story_id = id
                                                   WHERE person_id=ANY(?)
                                                         AND published_at >= ?
                                                         AND text IS NOT NULL
                                                         AND upvote_count >= 10"
                                            (map :id tranning-users) (DateTime. 2015 1 1 0 0)])
                                (map :id)
                                (frequencies)
                                )
        testing-doc-index->freq (->> testing-doc->index
                                     (sort-by second)
                                     (map #(or (story->frequencies (first %)) 0))

                                     )


        testing-Theta (m/matrix (map #(crawler/story->topic %) testing-docs))
        _ (println "Precompute done.")
        dat (pmap (fn [u]
                    (let [user-testing-doc-index
                          (->> (user-id->testing-stories (:id u) )
                               (into #{})
                               (clojure.set/intersection testing-docs-set)
                               (map testing-doc->index)
                               )
                          user-training-doc-index
                          (->> (user-id->training-stories (:id u) )
                               (into #{})
                               (clojure.set/intersection training-docs-set)
                               (map training-doc->index)
                               )

                          twitter-pref (crawler/twitter-name->twitter-profile (:tw u))

                          rank->summary (fn [scores u n method]
                                          (let [index-sort-by-score
                                                (->> scores
                                                     (map vector (range))
                                                     (sort-by second)
                                                     (reverse)
                                                     (map first)
                                                     )
                                                index->rank
                                                (->>
                                                  index-sort-by-score
                                                  (map (fn [rank index] (vector index rank)) (range))
                                                  (into {}))
                                                rank (map index->rank user-testing-doc-index)]
                                            {:recall-100 (k-recall 100 rank)
                                             :recall-50  (k-recall 50 rank)
                                             :top-100    (k-precision 100 rank)
                                             :top-50     (k-precision 50 rank)
                                             :rr         (rr rank)
                                             :nDCG_10    (nDCG 10 rank)
                                             :nDCG_50    (nDCG 50 rank)
                                             :MAP        (MAP rank)
                                             :method     method
                                             :user       u
                                             :n          n
                                             :rank       rank
                                             :entropy-100 (->> (take 100 index-sort-by-score)
                                                               (map #(m/get-row testing-Theta %))
                                                               (apply m/add)
                                                               (entropy)
                                                               )
                                             :entropy-50  (->> (take 50 index-sort-by-score)
                                                               (map #(m/get-row testing-Theta %))
                                                               (apply m/add)
                                                               (entropy)
                                                               )
                                             }
                                            )
                                          )
                          ]
                      [(->
                         (rank->summary testing-doc-index->freq u 0 :most-popular)
                         )
                       (->
                         (m/inner-product testing-Theta twitter-pref)
                         (rank->summary u 0 :content-based)
                         )
                       (-> (m/inner-product testing-V twitter-pref)
                           (rank->summary u 0 :ours)
                           )

                       (doall
                         (for [n (range 1 10)]
                           [
                            (->(fold-in twitter-pref training-V XX (take n user-training-doc-index) 10)
                               (->>
                                 (m/inner-product testing-V)
                                 )
                               (rank->summary u n :ours)
                               )

                            (->(fold-in twitter-pref training-V XX (take n user-training-doc-index) 1000)
                               (->>
                                 (m/inner-product testing-V)
                                 )
                               (rank->summary u n :ours-1000)
                               )

                            (->(fold-in (m/zero-vector 200) ctr-training-V ctr-XX (take n user-training-doc-index) 0.01)
                               (->>
                                 (m/inner-product ctr-testing-V)
                                 )
                               (rank->summary u n :ctr)
                               )
                            (->(fold-in (m/zero-vector 200) pmf-training-V pmf-XX (take n user-training-doc-index) 0.01)
                               (->>
                                 (m/inner-product pmf-testing-V)
                                 )
                               (rank->summary u n :pmf)
                               )
                            ]
                           ))
                       ]



                      ))
                  testing-users
                  )
        dat (to-dataset (flatten (doall dat)))
     ]

    (println (with-data  dat
                         (->> (aggregate [:top-100 :top-50 :rr :nDCG_10 :nDCG_50 :MAP :recall-100 :recall-50
                                          :entropy-100 :entropy-50] [:method :n] :rollup-fun stats/mean)
                              ($order [:n :method] :desc))

                         ))
    dat
    )
  )


(defn profile-corrs []
  (let [users   (->> (sql/query @pooled-db [
                                            "SELECT id, twitter_screen_name as tw, medium_user_name as u
                                            FROM person WHERE twitter_screen_name IS NOT NULL"])
                     (filter #(crawler/twitter-name->twitter-profile (:tw %)))
                     )
        user->stories (let [uid->stories (->> (sql/query @pooled-db
                                                         ["SELECT medium_story.id AS id, person_id AS uid
                                            FROM medium_story
                                            INNER JOIN medium_like ON story_id = id
                                            WHERE person_id=ANY(?)
                                                  AND text IS NOT NULL"
                                                          (map :id users) ])
                                              (map (fn [{:keys [uid id]}] {uid [id]}))
                                              (apply merge-with concat)
                                              )]
                        (fn [{:keys [id]}]
                          (uid->stories id)
                          )
                        )

        ]

    (->> (pmap (fn [u]
                 (try

                   (if  (> (count (user->stories u)) 9)
                     (->> (user->stories u)
                          (take 300)
                          (map crawler/story->topic)
                          (apply m/add)
                          (stats/correlation (crawler/twitter-name->twitter-profile (:tw u)))

                          ))
                   (catch Exception _ nil))
                 )
               users
               )
         (filter identity))

    ))

(def ret (json/parse-stream (clojure.java.io/reader "ours-pmf-ctr.json")  true))
(def dat (analysis (into {} (map (fn [[k v]] [(name k) v]) (second (:ours ret))))
                   (into {} (map (fn [[k v]] [(name k) v]) (second (:ctr ret))))
                   (into {} (map (fn [[k v]] [(name k) v]) (second (:pmf ret))))
                   (:training-docs ret) (:training-users ret)
                   (:testing-docs ret) (:testing-users ret)))



