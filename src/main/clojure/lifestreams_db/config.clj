(ns lifestreams-db.config
  (require [clojure.edn]
           [clojure.java.io])
  (:import (java.io PushbackReader)))

(def config
  (delay
    (with-open [rdr (PushbackReader. (clojure.java.io/reader "config.edn"))]
      (clojure.edn/read rdr))
    ))
