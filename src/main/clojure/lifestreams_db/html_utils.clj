(ns lifestreams-db.html-utils
  (:import (clojure.lang IPersistentMap ISeq)))

(defmulti extract-snippet-content class)
(defmethod extract-snippet-content IPersistentMap [{:keys [content]}]
  (mapcat extract-snippet-content content)
  )
(defmethod extract-snippet-content ISeq [coll]
  (mapcat extract-snippet-content coll)
  )
(defmethod extract-snippet-content String [s]
  [s]
  )
(defmethod extract-snippet-content :default [_]
  nil
  )


