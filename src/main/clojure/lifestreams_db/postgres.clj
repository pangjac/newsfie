(ns lifestreams-db.postgres
  (:import (java.net URI)
           (com.mchange.v2.c3p0 ComboPooledDataSource))
  (:require [clojure.java.jdbc :as sql]
            [clojure.string :as string]))

(def url "postgres://twitter:twitter@localhost/twitter")
(defn pool [url]
  (let [uri (URI. url)
        host (.getHost uri)
        port (if (pos? (.getPort uri)) (.getPort uri) 5432)
        path (.getPath uri)
        user-info (or (.getUserInfo uri) ":")
        [user password] (clojure.string/split user-info #":")]
    {:datasource
     (doto (ComboPooledDataSource.)
       (.setDriverClass "org.postgresql.Driver")
       (.setJdbcUrl (str "jdbc:postgresql://" host ":" port path))
       (.setInitialPoolSize 3)
       (.setMaxPoolSize 5000)
       (.setUser user)
       (.setPassword password))}))

(def pooled-db (delay (pool url)))