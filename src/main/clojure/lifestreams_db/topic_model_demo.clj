(ns lifestreams-db.topic-model-demo
  (:use [org.httpkit.server]
        [compojure.route :only [files not-found]]
        [compojure.handler :only [site api]]                ; form, query params decode; cookie; session, etc
        [compojure.core :only [defroutes GET POST DELETE ANY context]]
        [cheshire.core]
        [cheshire.generate :only [add-encoder encode-seq]]
        [lifestreams-db.user-state]
        )
  (:require [lifestreams-db.topic-model :as topics]
            [cheshire.core :as json]))


(def model (topics/load-model "medium-new.bin"))
(def topic-titles (topics/topic-titles model))


(defn infer-topics [content]
  (apply topics/infer-topic-dist model content 100 [8 33 54 62 64 115 118 119 179]))

(defroutes all-routes
           ;(GET "/" [] show-landing-page)
           (POST "/topics" [] (fn [{:keys [body]}]
                                {:headers  {"accept"        "application/json"
                                            "content-type"  "application/json"}
                                 :body (json/generate-string
                                         {:topics (->
                                                    (slurp body)
                                                    (json/parse-string true)
                                                    (:content)
                                                    (infer-topics)
                                                    )

                                          :titles topic-titles})}

                                )))            ;; all other, return 404

(def stop-server (run-server (-> (api #'all-routes)) {:port 8101}))
