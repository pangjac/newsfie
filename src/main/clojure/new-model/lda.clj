(ns new-model.lda
  (:use (new-model.model)))

(defn update
  [data
   [old-topic]
   [new-topic]
   ]
  (if (not= old-topic new-topic)
    (cond->
      data
      old-topic
      (assoc old-topic (dec (nth data old-topic)))
      new-topic
      (assoc new-topic (inc (nth data new-topic)))
      )
    data
    ))


(defrecord LDA [T W alpha beta]
  TopicModel
  (update-local-params [model [word] old new [topic->count]]
    [(update topic->count old new)]
    )
  (update-global-params [model [word] old new [word->topic->count topic->count]]
    [(assoc word->topic->count (nth word->topic->count word)
                               (update (nth word->topic->count word) old new))
     (update topic->count old new)]
    )
  (init-global-params [model]
    [
     ; word->topic->count
     (into [] (repeat W (into [] (repeat T beta))))
     ; topic->count
     (into [] (repeat T (* W beta)))
     ]
    )
  (init-local-params [model words]
    ; topic->count
    [(into [] (repeat T alpha))]

    )
  (init-word-params [model]
    [(rand-nth (range T))]
    )
  (sample [model [word] [old-topic] [local-topic->count] [word->topic->count topic->count]]
    (let [exclude-and-get
          (fn [data old-topic new-topic]
            (cond-> (nth data new-topic) (= old-topic new-topic) dec)
            )
          current-word->topic->count (nth word->topic->count word)
          probs (for [topic (range T)]
                  (/ (* (exclude-and-get current-word->topic->count old-topic topic)
                        (exclude-and-get local-topic->count old-topic topic)
                        )
                     (exclude-and-get topic->count old-topic topic)
                     )
                  )
          mass (apply + probs)
          sample (* (rand) mass)
          ]
      (reduce
        (fn [[cum-sum [prob & probs]] topic]
          (let [cum-sum (+ cum-sum prob)]
            (if (>= cum-sum sample)
              (reduced [topic])
              [cum-sum probs]
              ))
          )
        [0 probs]
        (range T)
        )
      )
    )
  )




