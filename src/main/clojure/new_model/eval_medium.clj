(ns new-model.eval-medium
  (:require [cheshire.core :as json]
            [lifestreams-db.data :as data]
            [lifestreams-db.postgres :as postgres]
            [incanter.core]
            [incanter.datasets]
            [clojure.core.matrix :as m]
            [experiment.analysis :as analysis]


            [incanter.stats :as stats])
  (:use
    [new-model.eval])
  )


(defonce medium-eval-info
  (->>
    (json/parse-stream (clojure.java.io/reader "medium-user->info.json") true)
    )
  )

(def medium-user->info
  (->> medium-eval-info
                :users
                (filter #(>= (count (:followees (second %))) 50))
                (into {})
                ))

(def stories (:stories medium-eval-info))



(def story->topic
         (memoize
           (fn [model-name id]
             (profile model-name id (:text (data/get-story-with-id @postgres/pooled-db id)) "News")
             )))


(defn user->pref [profile-fn {:keys [entity_id] :as user} pref-source]
  (let [hashtags  (take 300 (:hashtags (medium-user->info user)))
        followees (take 300 (:followees (medium-user->info user)))
        [topics own-count] (entity->topics-count profile-fn entity_id)]

    (->>

      (cond-> []
              (pref-source :own)
              (conj (m/mul topics own-count))
              (pref-source :hashtags)
              (conj (m/mul (pref-source :hashtags) (entities->topic-sum profile-fn hashtags)))
              (pref-source :followees)
              (conj (m/mul (pref-source :followees) (entities->topic-sum profile-fn followees)))
              )

      (apply m/add)
      (norm)
      (m/to-vector))
    )
  )

(defn eval-user [profile-fn user stories pref-source]
  (let [
  ;profile-fn (fn [& args] (p :profile (apply  args)))
        topics (user->pref profile-fn user pref-source)

        stories-topics (->> stories
                            (map (partial story->topic profile-fn))
                            (map norm)
                            (into [])
                            (m/matrix)
                            )
        scores (m/inner-product stories-topics topics)
        ]

    (->> (map #(array-map :score %1 :index %2) scores (range))
         (sort-by :score)
         (reverse)
         (map #(assoc %2 :rank %1) (range))
         (filter #(< (:index %) 10))
         (map :rank)
         )
    )
  )

(defn user-story-set [u]
  (let [user-stories (into #{} (:stories (medium-user->info u)))]
    (->>  (deterministic-shuffle stories (hash u))
          (filter (complement user-stories))
          (take 190)
          (concat (take 10 (deterministic-shuffle user-stories (hash u))))
          ))
  )
[:medium-100 :medium-200 :medium-400
 :multi-100 :multi-200 :multi-400
 :doc2vec-100 :doc2vec-200 :doc2vec-400]
{:own 1 :hashtags 1 :followees 1 }
{:own 1 :hashtags 1 :followees 2 }
{:own 1 :hashtags 1 :followees 5 }
(def results (->>
         medium-user->info
         (map first)
         (deterministic-shuffle)
         (take 5000)
         (pmap
           (fn [i u]
             (println i)
             (let [stories (user-story-set u)]
               (try
                 (->> (for [model [:medium-200
                                   :multi-200
                                   :doc2vec-200 ]]

                        (doall
                          (for [pref-source [
                                             {:own 1 :hashtags 0.5 :followees 5 }
                                             ]]
                            {:model model
                             :user u
                             :results (eval-user model u stories pref-source)
                             :pref-source pref-source
                             }))
                        )
                      (doall)

                      )
                 (catch Exception e (println e)))))

           (range))
         (filter identity)
         (flatten)
         (doall)
         ))

(->> (flatten
       (for [{:keys [results model user pref-source]} results]
         (let [pr-fn (analysis/pr-fn results)]

           (for [i (range 0.1 1.05 0.05)]
             {:method model
              :recall i
              :precision (pr-fn i)
              :pref (clojure.string/join "+" (map str pref-source))
              :user (:id user)
              }
             ))
         ))
     (incanter.core/to-dataset)
     (incanter.core/aggregate [ :precision] [:method :pref :recall] :rollup-fun stats/mean :dataset)
     (#(incanter.core/save % "medium-profile.csv"))
     )




