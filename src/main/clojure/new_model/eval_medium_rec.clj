(ns new-model.eval-medium-rec
  (:require [clojure.java.jdbc :as sql]
            [lifestreams-db.postgres :refer [pooled-db]]
            [new-model.eval-medium :as eval-medium]
            [new-model.models :as models]
            [new-model.eval :as eval]
            [cheshire.core :as json]
            [lifestreams-db.ctr :as ctr]
            [clojure.core.matrix :as m]
            [taoensso.nippy :as nippy]
            [cheshire.generate :refer [add-encoder encode-seq remove-encoder]])
  (:import (org.joda.time DateTime)
           (java.io DataInputStream DataOutputStream)
           (mikera.vectorz.impl AArrayVector)))


(def results
  (let [users (->> (sql/query @pooled-db
                              ["SELECT twitter_entity.id as entity_id, person.id as id, name as entity_name
                              FROM person
                              INNER JOIN twitter_entity on twitter_entity.name=twitter_screen_name
                              INNER JOIN twitter_entity_text on entity_id=twitter_entity.id
                             "])

                   )
        stories (->> (sql/query @pooled-db
                                ["SELECT medium_story.id AS id, person_id AS uid, published_at < ? AS training
                                            FROM medium_story
                                            INNER JOIN medium_like ON story_id = id
                                            WHERE person_id=ANY(?)
                                                  AND text IS NOT NULL
                                                  AND upvote_count >= 10"
                                 (DateTime. 2015 1 1 0 0) (into [] (map :id users)) ])

                     )
        user->training (->> stories
                            (filter :training)
                            (map (fn [{:keys [uid id]}] {uid [id]}))
                            (apply merge-with concat)
                            )
        user->testing (->> stories
                           (filter (complement :training))
                           (map (fn [{:keys [uid id]}] {uid [id]}))
                           (apply merge-with concat)
                           )

        testing-users
        (->> users
             (eval/deterministic-shuffle)
             (filter (fn [{:keys [id]}]
                       (and
                         (user->training id)
                         (>= (count (user->training id)) 10)
                         (user->testing id)
                         (>= (count (user->testing id)) 5))
                       ))

             (take 5000)
             )
        training-users (let [testing? (into #{} testing-users)]
                         (filter (complement testing?) users)
                         )

        training-docs
        (->> (mapcat
               (fn [u] (user->training (:id u)))
               users)
             (distinct))
        testing-docs
        (->> (mapcat
               (fn [u] (user->testing (:id u)))
               users)
             (distinct))
        docs (concat training-docs testing-docs)
        user->stories #(distinct (concat (user->training %)
                                         (user->testing %)))


        docs
        (distinct (concat (mapcat second user->training) (mapcat second user->testing)))

        user-doc (apply concat
                        (for [u training-users]
                          (for [s (user->stories (:id u))]
                            [u s]
                            )

                          ))

        Theta (into {} (pmap #(vector % (eval-medium/story->topic :multi-200 %)) docs))
        Gamma (into {} (pmap #(vector % (eval/entity-name->profile :multi-200 (:entity_name %))) training-users))


        [_ pmf]
        (ctr/estimate
          (into {} (pmap #(vector % (m/zero-vector 200)) docs))
          (into {} (pmap #(vector % (m/zero-vector 200)) training-users))
          user-doc
          1
          0.01
          0.01
          0.01
          5
          )
        [_ ours]
        (ctr/estimate Theta
                      Gamma
                      user-doc
                      1
                      0.01
                      10
                      10
                      10
                      )


        [_ ctr]
        (ctr/estimate Theta
                      (into {} (pmap #(vector % (m/zero-vector 200)) training-users))
                      user-doc
                      1
                      0.01
                      10
                      0.01
                      5
                      )
        vec-to-seq (fn [m] (into {} (map #(assoc % 1 (seq (second %)))) m))
        ]

    {:ours (vec-to-seq ours)
     :ctr (vec-to-seq ctr)
     :pmf (vec-to-seq pmf)
     :theta (vec-to-seq Theta)
     :gamma (vec-to-seq Gamma)
     :testing-users testing-users
     :training-users training-users
     :user->training user->training
     :user->testing user->testing
     :training-docs training-docs
     :testing-docs testing-docs
     }

    )
  )

(with-open [w (clojure.java.io/writer "medium-rec-eval.edn")]
  (binding [*print-length* false
            *out* w]
    (pr results)))
