(ns new-model.lda-background
  (:use [new-model.model]
        [new-model.utils])
  (:require [new-model.model :refer [TopicModel]])

  )


(def TOPICAL 0)
(def BACKGROUND 1)

(defrecord LDABackground [T W alpha beta gamma index->token]
  TopicModel
  (empty-word-params [model]
    [nil nil]
    )
  (init-local-cache [model local global]
    nil)
  (init-local-params [model]
    [; topic->count
     (into [] (repeat T alpha))
     ; [TOPICAL+gamma BACKGROUND+gamma]
     [gamma gamma]
     ]
    )
  (init-global-params [model]
    [; [TOPICAL BACKGROUND]
     [(* W beta) (* W beta)]
     ; word->topic->count
     (into [] (repeat W (into [] (repeat T beta))))
     ; word->type->-count
     (into [] (repeat W (into [] (repeat 2 beta))))
     ; topic->count
     (into [] (repeat T (* W beta)))
     ]
    )
  (update-local-cache [model token _ _  _
                       _ _ ]
    nil)
  (update-local-params [model word [old-type old-topic] [new-type new-topic] [topic->count type->count]]
    [(update-topic-map topic->count old-topic new-topic)
     (update-topic-map type->count old-type new-type)
     ]
    )
  (update-global-params [model word [old-type old-topic] [new-type new-topic] [type->count word->topic->count word->type->count topic->count]]
    [(update-topic-map type->count old-type new-type)
     (assoc word->topic->count word
                               (update-topic-map (nth word->topic->count word) old-topic new-topic))
     (assoc word->type->count word
                              (update-topic-map (nth word->type->count word) old-type new-type))
     (update-topic-map topic->count old-topic new-topic)]
    )
  (sample [model word _
           [local-topic->count local-type->count]
           [type->count word->topic->count word->type->count topic->count]]
    (let [current-word->topic->count (word->topic->count word)
          probs (cons
                  (* (/ (local-type->count BACKGROUND)
                        (local-type->count TOPICAL))
                     (/ ((nth word->type->count word) BACKGROUND)
                        (type->count BACKGROUND))
                     (+ (- (local-type->count TOPICAL)
                           gamma)
                        (* T alpha))

                     )
                  (for [topic (range T)]
                    (/ (* (current-word->topic->count topic)
                          (local-topic->count topic)
                          )
                       (topic->count topic)
                       )
                    ))
          possible-outcomes (cons
                              [BACKGROUND nil]
                              (for [t (range T)]
                                [TOPICAL t])
                              )
          mass (apply + probs)
          sample (* (rand) mass)
          ]
      (position sample possible-outcomes probs)
      )
    )
  (diagnose [model [type->count word->topic->count word->type->count topic->count]]
    (->>
      [(for [[word-index topic-counts] (map vector (range) word->topic->count)]
         (for [[topic-index count] (map vector (range) topic-counts)]
           {:topic topic-index :word word-index :count count}
           )

         )
       (for [[word-index counts] (map vector (range) word->type->count)]
         {:topic :BACKGROUND :word word-index :count (nth counts BACKGROUND)}
         )
       ]
      (flatten)
      (group-by :topic)
      (sort-by (fn [[topic _]]
                 (if (= topic :BACKGROUND)
                   -1 topic)))
      (pmap (fn [[t ws]]
              (let [keywords (map (comp index->token :word) (take 4 (reverse (sort-by :count ws))))]
                (str t ":" (clojure.string/join "," keywords))
                )))
      (clojure.string/join "\n")
      (println)
      )
    )
  )
