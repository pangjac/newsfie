(ns new-model.model-runner
  (:require [lifestreams-db.data :as data]
            [clojure.java.jdbc :as sql]
<<<<<<< HEAD
            [new-model.fast-lda-background-multi-corpus :refer (->FastLDABackgroundMultiCorpus)]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.core.reducers :as r])
=======
            [new-model.fast-lda :refer (->FastLDA)]
            [new-model.lda :refer (->LDA)]
            [new-model.fast-lda-background :refer (->FastLDABackground)]
            [new-model.lda-background :refer (->LDABackground)]
            [taoensso.timbre.profiling
             :refer (pspy pspy* profile defnp p p*)]
            )
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
  (:use [new-model.model])
  (:import (org.tartarus.snowball.ext EnglishStemmer)
           ))





(defn story->words [{:keys [text tags]}]
  (let [stemmer (EnglishStemmer.)
        stem (fn [word]
               (.setCurrent stemmer word)
               (.stem stemmer)
               (.getCurrent stemmer)
               )]
    (concat
      ; text
      (->> (re-seq #"\p{L}\p{L}\p{L}+" text)
           (map clojure.string/lower-case)
           (map stem)
           )
      ;tag
      (->> (map :name tags)
           (map (partial str "#")))
      ))

  )
(defn stop-words [doc-tokens]
<<<<<<< HEAD
  (let [threshold (* 0.1 (count doc-tokens))]
    (->> doc-tokens
         (r/map distinct)
         (into [])
         (apply concat)
         (reduce (fn [[freqs stopword-set] word]

                   (if (stopword-set word)
                     [freqs stopword-set]
                     (let [freq (inc (or (freqs word) 0))]
                       (if (> freq threshold)
                         [(dissoc! freqs word) (conj! stopword-set word)]
                         [(assoc! freqs word freq) stopword-set]
                         )
                       )

                     )
                   )
                 [(transient {}) (transient #{})]
                 )

         (second)
         (persistent!)
         )
    )

  )

(defn sample-conditional [model token word-param empty-word-param local-cache local-params corpus-params global-params]

  (sample model token
          (update-local-cache model token word-param empty-word-param local-cache local-params corpus-params global-params)
          (update-local-params model token word-param empty-word-param local-params)
          (update-corpus-params model token word-param empty-word-param corpus-params)
=======
  (->> doc-tokens
       (map distinct)
       (apply concat)
       (frequencies)
       (filter (fn [[_ freq]]
                 (> freq (* 0.1 (count doc-tokens)))
                 ))
       (map first)
       )
  )

(defn sample-conditional [model token word-param empty-word-param local-cache local-params global-params]
  (sample model token
          (update-local-cache model token word-param empty-word-param local-cache local-params global-params)
          (update-local-params model token word-param empty-word-param local-params)
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
          (update-global-params model token word-param empty-word-param global-params)
          )
  )

<<<<<<< HEAD
(defn doc-iteration [model tokens word-params-vec local-params corpus-params global-params]
  (let [empty-word-param (empty-word-params model)
        local-cache (init-local-cache model local-params corpus-params global-params)
        [new-word-params _ new-local new-corpus new-global]
        (reduce
          (fn [[new-word-param-vec local-cache  local-params corpus-params global-params] [token word-param]]
            (let [new-word-param (sample-conditional model token word-param empty-word-param local-cache local-params  corpus-params global-params)]
              [(conj! new-word-param-vec new-word-param)
               (update-local-cache model token word-param new-word-param local-cache local-params corpus-params global-params )
               (update-local-params model token word-param new-word-param local-params)
               (update-corpus-params model token word-param new-word-param corpus-params)
=======
(defn doc-iteration [model tokens word-params-vec local-params global-params ]
  (let [empty-word-param (empty-word-params model)
        local-cache (init-local-cache model local-params global-params)
        [new-word-params _ new-local new-global]
        (reduce
          (fn [[new-word-param-vec local-cache  local-params  global-params] [token word-param]]
            (let [new-word-param (sample-conditional model token word-param empty-word-param local-cache local-params  global-params)]
              [(conj! new-word-param-vec new-word-param)
               (update-local-cache model token word-param new-word-param local-cache local-params global-params )
               (update-local-params model token word-param new-word-param local-params)
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
               (update-global-params model token word-param new-word-param global-params)
               ]
              )
            )
<<<<<<< HEAD
          [(transient []) local-cache local-params corpus-params global-params]
          (map vector tokens word-params-vec)
          )
        ]
    [(persistent! new-word-params) new-local new-corpus new-global]

    )
  )
(defn corpus-iteration [model doc-tokens doc-word-params doc-local-params corpus-params global-params]
  (let [[new-doc-word-params new-doc-local-params corpus-params global-params]
        (reduce
          (fn [[new-doc-word-params new-doc-local-params corpus-params global-params]
               [tokens word-params local-params]]
            (let [[new-word-params new-local new-corpus new-global]
                  (doc-iteration model tokens word-params local-params corpus-params global-params)]
              [(conj! new-doc-word-params new-word-params)
               (conj! new-doc-local-params new-local)
               new-corpus
               new-global]
              )
            )
          [(transient []) (transient []) corpus-params global-params]
          (map vector doc-tokens doc-word-params doc-local-params)
          )
        ]
    [(persistent! new-doc-word-params) (persistent! new-doc-local-params) corpus-params global-params]
    )

  )
(defn one-iteration-serial [model
                            corpus->doc->tokens
                            corpus->doc->word-params
                            corpus->doc->local-params
                            corpus->corpus-params
                            global-params]
  (reduce
    (fn [[corpus->doc->word-params corpus->doc->local-params corpus->corpus-params global-params]
         [doc-tokens doc-word-params doc-local-params corpus-params]]
      (let
        [[new-doc-word-params new-doc-local-params new-corpus-params new-global-params]
         (corpus-iteration model doc-tokens doc-word-params doc-local-params corpus-params global-params)]
        [(conj corpus->doc->word-params new-doc-word-params)
         (conj corpus->doc->local-params new-doc-local-params)
         (conj corpus->corpus-params new-corpus-params)
         new-global-params
         ]
        )
      )
    [[] [] [] global-params]
    (map vector corpus->doc->tokens corpus->doc->word-params corpus->doc->local-params corpus->corpus-params)
=======
          [(transient []) local-cache local-params global-params]
          (map vector tokens word-params-vec)
          )
        ]
    [(persistent! new-word-params) new-local new-global]

    )
  )
(defn one-iteration-serial [model doc-tokens doc-word-params doc-local-params global-params]
  (reduce
    (fn [[new-doc-word-params new-doc-local-params global-params ]
         [tokens word-params local-params  ]]
      (let [[new-word-params new-local new-global ]
            (doc-iteration model tokens word-params local-params global-params )]
        [(conj new-doc-word-params new-word-params)
         (conj new-doc-local-params new-local)
         new-global]
        )
      )
    [[] [] global-params ]
    (map vector doc-tokens doc-word-params doc-local-params)
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
    )
  )


<<<<<<< HEAD
(defn corpus-iteration-parallel [model doc-tokens doc-word-params doc-local-params corpus-params global-params]
=======
(defn one-iteration-parallel [model doc-tokens doc-word-params doc-local-params global-params]
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
  (let [chunk-size (max 512 (int (+ 1 (/ (count doc-tokens) 32))))
        results
        (pmap
          (fn [sub-doc-tokens sub-doc-word-params sub-doc-local-params]
<<<<<<< HEAD
            (corpus-iteration model sub-doc-tokens sub-doc-word-params sub-doc-local-params corpus-params global-params)
=======
            (one-iteration-serial model sub-doc-tokens sub-doc-word-params sub-doc-local-params global-params)
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
            )
          (partition-all  chunk-size doc-tokens)
          (partition-all  chunk-size doc-word-params)
          (partition-all  chunk-size doc-local-params)
          )
        new-doc-word-params
        (mapcat first results)
        new-local-params
        (mapcat second results)
<<<<<<< HEAD
        [new-corpus-params new-global-params]
        (reduce (fn [[new-corpus new-global] [token old new]]
                  [(update-corpus-params model token old new new-corpus)
                   (update-global-params model token old new new-global)]
                  )
                [corpus-params global-params]
=======
        new-global-params
        (reduce (fn [new-global [token old new]]
                  (update-global-params model token old new new-global)
                  )
                global-params
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
                (map vector
                     (apply concat doc-tokens)
                     (apply concat doc-word-params)
                     (apply concat new-doc-word-params))
                )
        ]
<<<<<<< HEAD
    [new-doc-word-params new-local-params new-corpus-params new-global-params]
    )
  )



(defn one-iteration-parallel [model
                            corpus->doc->tokens
                            corpus->doc->word-params
                            corpus->doc->local-params
                            corpus->corpus-params
                            global-params]
  (reduce
    (fn [[corpus->doc->word-params corpus->doc->local-params corpus->corpus-params global-params]
         [doc-tokens doc-word-params doc-local-params corpus-params]]
      (let
        [[new-doc-word-params new-doc-local-params new-corpus-params new-global-params]
         (corpus-iteration-parallel model doc-tokens doc-word-params doc-local-params corpus-params global-params)]
        [(conj corpus->doc->word-params new-doc-word-params)
         (conj corpus->doc->local-params new-doc-local-params)
         (conj corpus->corpus-params new-corpus-params)
         new-global-params
         ]
        )
      )
    [[] [] [] global-params]
    (map vector corpus->doc->tokens corpus->doc->word-params corpus->doc->local-params corpus->corpus-params)
=======
    [new-doc-word-params new-local-params new-global-params]

>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
    )
  )


<<<<<<< HEAD
=======

>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
(def results
  (let [doc-words (->>
                    (sql/query @lifestreams-db.postgres/pooled-db
                               ["Select json->'tags' as tags, text from medium_story limit 20000;"])
<<<<<<< HEAD
                    (r/map story->words)
                    (into [])
=======
                    (pmap story->words)


>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d
                    )
        _ (println "Corpus Size: " (count doc-words))
        stop-words? (into #{} (stop-words doc-words))
        _ (println "Stopwords:" (clojure.string/join " " stop-words?))
        doc-words (for [tokens doc-words] (filter (complement stop-words?) tokens))
        words (->> (flatten doc-words)
                   (distinct))
<<<<<<< HEAD
        T 200
        word->index (into {} (map vector words (range)))
        index->word (into {} (map vector (range) words))
        _ (println "T" T "W" (count words))
        model (->FastLDABackgroundMultiCorpus T (count words) 0.01 0.01 0.5 index->word)
        ; randomly initialize word tuples
        corpus->doc->tokens
        (partition-all 10000
                   (for [words doc-words]
                     (for [word words]
                       (word->index word)
                       )
                     ))
        corpus->doc->word-params
        (for [doc->tokens corpus->doc->tokens]
          (for [tokens doc->tokens]
            (for [token tokens]
              (empty-word-params model)
              )
            )
          )
        global-params
        (init-global-params model)
        corpus->corpus-params
        (for [_ corpus->doc->tokens]
          (init-corpus-params model)
          )
        corpus->doc->local-params
        (for [doc->tokens corpus->doc->tokens]
          (for [_ doc->tokens]
            (init-local-params model)
            )
          )
        num-iteration 100
        [corpus->doc->word-params  corpus->doc->local-params corpus->corpus-params global-params]
        (reduce (fn [params iteration-num]
                  (println iteration-num)
                  (profile :info :one-parallel
                           (apply one-iteration-parallel model corpus->doc->tokens params)))
                [corpus->doc->word-params  corpus->doc->local-params corpus->corpus-params global-params]
                (range num-iteration)
                )
        ]

    (->>
      (diagnose model corpus->corpus-params global-params))
    [model corpus->corpus-params global-params]
=======
        T 300
        word->index (into {} (map vector words (range)))
        index->word (into {} (map vector (range) words))
        _ (println "T" T "W" (count words))
        model (->FastLDABackground T (count words) 0.1 0.01 0.5 index->word)
        ; randomly initialize word tuples
        doc->tokens
        (for [words doc-words]
          (for [word words]
            (word->index word)
            )
          )
        doc->word-params
        (for [tokens doc->tokens]
          (for [token tokens]
            (empty-word-params model)
            )
          )
        global-params
        (reduce (fn [global-params [token params]]
                  (update-global-params model token nil params global-params)
                  )
                (init-global-params model)
                (map vector
                     (mapcat identity doc->tokens)
                     (mapcat identity doc->word-params)))
        doc->local-params
        (for [[tokens params] (map vector doc->tokens doc->word-params)]
          (reduce (fn [local-params [token params]]
                    (update-local-params model token nil params local-params))
                  (init-local-params model)
                  (map vector tokens params)
                  )
          )
        num-iteration 200
        restults (reduce (fn [[doc->word-params  doc->local-params global-params] iteration-num]
                           (println iteration-num)
                           (profile :info :one-iteration
                                    (one-iteration-parallel model doc->tokens doc->word-params doc->local-params global-params)))
                         [doc->word-params  doc->local-params global-params]
                         (range num-iteration)
                         )
        ]

    (->>

      (last restults)
      (diagnose model))
    [model restults]
>>>>>>> 589be33e659cf3a92511bf8429dd359351aea06d


    ))