(ns new-model.utils)

(defn update-topic-map
  [data
   old-topic
   new-topic
   ]
  (if (not= old-topic new-topic)
    (cond->
      data
      old-topic
      (assoc old-topic (dec (nth data old-topic)))
      new-topic
      (assoc new-topic (inc (nth data new-topic)))
      )
    data
    ))


(defn position [val cases weights]
  (reduce
    (fn [[cum-sum [w & weights]] case]
      (let [cum-sum (+ cum-sum w)]
        (if (>= cum-sum val)
          (reduced case)
          [cum-sum weights]
          ))
      )
    [0 weights]
    cases))




