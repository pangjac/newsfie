(ns pinterest.pinterest
  (:require [org.httpkit.client :as http]
            [clj-time.core :as t]
            [taoensso.timbre :as timbre
             :refer (log info warn error trace)]
            [clojure.string :as str]
            [cheshire.core :as json]
            [taoensso.timbre.profiling :refer (pspy pspy* profile defnp p p*)]
            [amazonica.aws.s3 :as s3]
            [monger.core :as mg]
            [monger.collection :as mc])

  (:import (java.io ByteArrayInputStream Writer)
           (com.amazonaws.services.s3.model AmazonS3Exception)
           (org.joda.time.format DateTimeFormat)))



(def conn (mg/connect))
(def db (mg/get-db conn "pinterest"))

(defn- double-escape [^String x]
  (.replace (.replace x "\\" "\\\\") "$" "\\$"))
(def ^:private string-replace-bug?
  (= "x" (str/replace "x" #"." (fn [x] "$0"))))
(defmacro ^:no-doc fix-string-replace-bug [x]
  (if string-replace-bug?
    `(double-escape ~x)
    x))
(defn percent-encode
  "Percent-encode every character in the given string using either the specified
  encoding, or UTF-8 by default."
  [^String unencoded & [^String encoding]]
  (->> (.getBytes unencoded (or encoding "UTF-8"))
       (map (partial format "%%%02X"))
       (str/join)))

(defn url-encode
  "Returns the url-encoded version of the given string, using either a specified
  encoding or UTF-8 by default."
  [unencoded & [encoding]]
  (str/replace
    unencoded
    #"[^A-Za-z0-9_~.+-]+"
    #(double-escape (percent-encode % encoding))))

(def get-cred
  (fn []
    {:access-key "AKIAIZFTDH6D6BMCX5EA"
     :secret-key "JnlfX2d+So/pNPlC7cu9LEGXgC3d4h1fXX2T64Ta"
     :endpoint   "us-east-1"}

    )
  )


(defn endpoint-local
  "Make http request to ptt.cc via distributed proxies"
  [path data]
  (let [url (str "http://www.pinterest.com" path "?data=" (url-encode (json/generate-string data)))]
    (let [ret @(http/get url
                         {:headers    {"X-Requested-With" "XMLHttpRequest"}
                          :basic-auth ["pttrocks" "Cens0123!"]
                          :timeout    10000                 ; ms
                          :user-agent "Mozilla/5.0 (iPad; CPU OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) CriOS/30.0.1599.12 Mobile/11A465 Safari/8536.25 (3B92C18B-D9DE-4CB7-A02A-22FD2AF17C8F)"
                          :insecure?  true}
                         )]
      (Thread/sleep (* 5 1000))
      ret
      )
    )

  )


(defn pinterest-sequence [path data & [bookmark]]

  (let [old-bookmark bookmark
        {:keys [body status]}
        (endpoint-local path (assoc-in data ["options" "bookmarks"]
                                       (if bookmark [bookmark] [])))]
    (if-not (#{200 404} status)
      (throw (Exception. (str status body)))
      (let [{:keys [resource resource_response]} (json/parse-string body true)
            bookmark (first (get-in resource [:options :bookmarks]))]
        (if (and bookmark (not= bookmark "-end-") (not= bookmark old-bookmark))
          (concat (:data resource_response) (lazy-seq (pinterest-sequence path data bookmark)))
          (:data resource_response)
          )

        )

      )
    )
  )
(defn board-pins [board-id]
  (->> (pinterest-sequence "/resource/BoardFeedResource/get/"
                           {"options" {"board_id"               (str board-id),
                                       "add_pin_rep_with_place" :null,
                                       "page_size"              :200,
                                       "prepend"                :true,
                                       "access"                 [],
                                       "board_layout"           "default"},
                            "context" {}})
       (filter #(= "pin" (:type %))))

  )
(defn query [term scope]
  (pinterest-sequence "/resource/SearchResource/get/"
                      {"options" {"query"        term,
                                  "scope"        scope
                                  "page_size"    :-1,
                                  "prepend"      :true,
                                  "access"       [],
                                  "board_layout" "default"},
                       "context" {}})
  ;?data={"options":{"layout":null,"places":null,"constraint_string":null,"show_scope_selector":true,"query":"travel","scope":"boards","bookmarks":[], "page_size":-1},"context":{}}
  )


(defn user-boards [user]
  (pinterest-sequence "/resource/ProfileBoardsResource/get/"
                      {"options" {"field_set_key" "grid_item"
                                  "username"      user},
                       "context" {}})
  ;?data={"options":{"layout":null,"places":null,"constraint_string":null,"show_scope_selector":true,"query":"travel","scope":"boards","bookmarks":[], "page_size":-1},"context":{}}
  )

(defn category-feed [category]
  (->>
    (pinterest-sequence "/resource/CategoryFeedResource/get/"
                        {"options" {"feed" category},
                         "context" {}})
    (filter #(= (:type %) "pin")))
  )
(defn discover-boards [term category]
  (->>
    (category-feed category)
    (map :board)
    (filter #(.contains (clojure.string/lower-case (or (:name %) "")) term))
    )
  )

(defn update? [filename legnth]
  (try
    (not= legnth (:instance-length
                   (s3/get-object-metadata (get-cred)
                                           :bucket-name "cornell-nyc-sdl-pinterest-image"
                                           :key filename)))
    (catch AmazonS3Exception e
      (do (trace "key" filename "does not exist")
          true
          ))
    )
  )

(defn exist? [filename]
  (try
    (s3/get-object-metadata (get-cred)
                            :bucket-name "cornell-nyc-sdl-pinterest-image"
                            :key filename)
    true
    (catch AmazonS3Exception e
      (do (trace "key" filename "does not exist")
          false
          ))
    )
  )

(defn get-all-exisitng-images [& [next-marker]]
  (let [{:keys [object-summaries next-marker]}
        (s3/list-objects (get-cred)
                         :bucket-name "cornell-nyc-sdl-pinterest-image"
                         :marker next-marker
                         )]
    (cond->
      (map :key object-summaries)
      next-marker
      (concat (lazy-seq (get-all-exisitng-images next-marker)))
      )
    )
  )

(defn upload-to-s3 [filename input length]
  (if (update? filename length)
    (s3/put-object (get-cred)
                   :bucket-name "cornell-nyc-sdl-pinterest-image"
                   :key filename
                   :input-stream input
                   :metadata {:content-length length}
                   :return-values "ALL_OLD")
    )
  )
(defn str->time [s]
  (.parseDateTime (DateTimeFormat/forPattern "EEE, dd MMMM y HH:mm:ss Z") s)
  )

(defn pin->filename [{:keys [id images] :as pin}]
  (let [url (:url (:orig images))
        extension (last (filter seq (clojure.string/split url #"\.")))
        filename (str id "." extension)
        ]
    filename
    ))

(defn pin->url [{:keys [id images] :as pin}]
  (:url (:orig images)))


(defn download-pin-image [{:keys [id images] :as pin}]
  @(http/get (pin->url pin)))

(defn download-board-pins [id]
  (->> (board-pins id)
       (pmap (fn [{:keys [images] :as pin}]
               (let [filename (pin->filename pin)
                     {:keys [body headers status] :as ret} (download-pin-image pin)]
                 (if (= 200 status)
                   (do
                     (upload-to-s3 filename body (Integer/parseInt (:content-length headers)))
                     (assoc pin :filename filename)
                     )
                   ;(throw (Exception. (str ret)))
                   )

                 ))
             )
       (filter identity)
       )
  )

(defn download-boards-with-term [term category]
  (doseq [{:keys [id] :as board} (->> (concat (discover-boards term category) (query term "boards"))
                                      (distinct)

                                      (filter #(not (mc/find-map-by-id db "pins" (:id %) ["_id"]))))]
    (try
      (mc/save db "pins" {:board      board
                          :pins-data  (download-board-pins id)
                          :query-term term
                          :_id        (:id board)})
      (println board)
      (catch Exception e (error e)))

    )
  )
(comment (def exist? (into #{} (get-all-exisitng-images))))


(defn output [term]
  (let [name (str term "_boards.jsonl")]
    (with-open [w (clojure.java.io/writer name :append false)]
      (doseq [row (mc/find-maps db "pins" {:query-term term :pins-data.99 {:$exists true}})]

        (.write w (str (json/generate-string
                         (->> (:pins-data row)
                              (map #(dissoc % :images :pinner :board))
                              (map #(assoc % :timestamp (str (str->time (:created_at %)))))
                              (assoc row :pins-data)
                              )
                         ) "\n"))
        ))
    (s3/put-object (get-cred)
                   :bucket-name "cornell-nyc-sdl-pinterest-metadata"
                   :key name
                   :input-stream (clojure.java.io/input-stream name)
                   :metadata {:content-length (.length (clojure.java.io/file name))}
                   :return-values "ALL_OLD")
    )
  )

(let [term "food"
      name (str term "_boards.jsonl")]

  (s3/put-object (get-cred)
                 :bucket-name "cornell-nyc-sdl-pinterest-metadata"
                 :key name
                 :input-stream (clojure.java.io/input-stream name)
                 :metadata {:content-length (.length (clojure.java.io/file name))}
                 :return-values "ALL_OLD")
  )






